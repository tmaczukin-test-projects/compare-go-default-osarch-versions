#!/bin/sh

versionsFile="versions.txt"
ciFile="${1:-.versions.gitlab-ci.yml}"

template() {
    version="${1}"

    cat <<EOS
collect ${version}:
  extends: .collect
  variables:
    VERSION: "${version}"

compare ${version}:
  extends: .compare
  variables:
    VERSION: "${version}"

EOS
}

echo "" > "${ciFile}"

while read -r version; do
    echo "Adding ${version}"
    template "${version}" >> "${ciFile}"
done < "${versionsFile}"
